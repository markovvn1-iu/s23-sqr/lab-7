# Main task

Run database and pgadmin. All tables will be created automatically:

```bash
docker-compose up -d
```

Now you can run the program.