import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
add_to_inventory = "INSERT INTO Inventory (username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s) on CONFLICT (username, product) DO UPDATE SET amount = Inventory.amount + %(amount)s"
inventory_amount_sum = "SELECT sum(amount) as total FROM Inventory WHERE username = %(username)s"

INVENTORY_LIMIT_PER_USER = 100


def get_connection():
    return psycopg2.connect(
        dbname="lab",
        user="user",
        password="pass",
        host="0.0.0.0",
        port=5333
    )  # TODO add your values here


def buy_product(username, product, amount):
    if not isinstance(amount, int) or amount <= 0:
        raise Exception('Incorrect amount value')

    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn, conn.cursor() as cur:
        try:
            cur.execute(buy_decrease_balance, obj)
            if cur.rowcount != 1:
                raise Exception("Wrong username")
        except psycopg2.errors.CheckViolation as e:
            raise Exception("Bad balance")
        try:
            cur.execute(buy_decrease_stock, obj)
            if cur.rowcount != 1:
                raise Exception("Wrong product or out of stock")
        except psycopg2.errors.CheckViolation as e:
            raise Exception("Product is out of stock")

        cur.execute(inventory_amount_sum, obj)
        total = cur.fetchone()[0] or 0
        if total + amount > INVENTORY_LIMIT_PER_USER:
            raise Exception("Too many products in Inventory")

        cur.execute(add_to_inventory, obj)
        if cur.rowcount != 1:
            raise Exception("Failed to update")


buy_product('Alice', 'marshmello', 1)