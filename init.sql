\c lab
CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
CREATE TABLE Inventory (username TEXT, product TEXT, amount INT, UNIQUE(username, product));
